import Header from './containers/Header';
import Body from './containers/Body';
import Footer from './containers/Footer';

function App() {
  return (
    <div>
      <Header />
      <Body />
      <Footer />
    </div>
  );
}

export default App;

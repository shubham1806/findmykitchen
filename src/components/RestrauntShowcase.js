import React from 'react';
class RestaurantShowcaseCard extends React.Component {
    render() {
        let bookBtnHtml;
        let orderBtnHtml;
        
        if (typeof (this.props.restaurant.book_url) === "undefined") {
            bookBtnHtml = <div target="_blank" disabled={true} className={"actionBtn notAvailableBtn"}>Booking not available</div>
        } else {
            bookBtnHtml = <a href={this.props.restaurant.book_url} rel="noreferrer" target="_blank" className={"actionBtn primary "}>Book Now</a>
        }
        if (typeof (this.props.restaurant.order_url) === "undefined") {
            orderBtnHtml = <div target="_blank" disabled={true} className={"actionBtn notAvailableBtn"}>Online Order not available</div>
        } else {
            orderBtnHtml = <a href={this.props.restaurant.order_url} rel="noreferrer" target="_blank" className={"actionBtn "}>Order Online</a>
        }
        return (
            <div className="restaurantContainer">
                <img src={this.props.restaurant.thumb !== "" ? this.props.restaurant.thumb: "/restaurant_default.jpg"} alt="Butterfly High" />
                <a href={this.props.restaurant.url} rel="noreferrer" target="_blank"><h3>{this.props.restaurant.name}</h3></a>
                <div className="cuisinesWrapper">
                    {this.props.restaurant.cuisines.split(", ").map((element, i) => {
                        return <span key={i} className="cuisine">{element}</span>
                    })}
                </div>
                <address className="restaurantAddress">{this.props.restaurant.location.address}</address>
                <p className="rating">Rating: {this.props.restaurant.user_rating.aggregate_rating}</p>
                {bookBtnHtml}
                {orderBtnHtml}
            </div>
        );
    }

}

class RestaurantShowcase extends React.Component {

    render() {
        
        if (this.props.restaurants.length > 0) {
            return (
                <div className="restaurantsResultsContainer">
                    {this.props.restaurants.map((element, i) => {
                        return <RestaurantShowcaseCard key={i} restaurant={element.restaurant} />
                    })}
                </div>
            )
        } else if(!this.props.resultsFound) {
            return (
                <div className="restaurantsResultsContainer">
                    <p className="restaurantLoading">No results found</p>
                </div>
            )
        } else {
            return (
                <div className="restaurantsResultsContainer">
                    <p className="restaurantLoading">Loading ...</p>
                </div>
            )
        }

    }

}

export default RestaurantShowcase;
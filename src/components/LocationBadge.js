import React from 'react';

function LocationBadge(props) {
    return (
        <div>
            <h5 className="locationBadge">Current Location: {props.location} {props.usingDefaultLocation ? "(Default)" : ""}</h5>
            {props.usingDefaultLocation ? <p className="note">Note: Please enable your location to get nearby restaurants</p> : ""}
        </div>
        
    )
}

export default LocationBadge;
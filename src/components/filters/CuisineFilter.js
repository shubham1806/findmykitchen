import React from 'react';

class CuisineCheckBoxFilter extends React.Component {

    render() {
        return (
            <div className="checkBoxFilter">
                <input type="checkbox" name="cuisine" id={this.props.cuisine} value={this.props.cuisine} onClick={this.props.ChangeHandler}/>
                <label htmlFor={this.props.cuisine}>{this.props.cuisine}</label>
            </div>
        )
    }
}
class CuisineFilter extends React.Component {
    render() {
        console.log(this.props);
        return (
            <div className="column">
                <h3>Cuisines</h3>
                <div className="FilterContainer">
                    {this.props.cuisines.map((element, i) => {
                        return <CuisineCheckBoxFilter key={i} cuisine={element} ChangeHandler={this.props.ChangeHandler}/>
                    })}
                </div>
            </div>
        )
    }
    
}

export default CuisineFilter;
import React from 'react';
import LocationBadge from '../LocationBadge';

class TextFilter extends React.Component {
    render() {
        return (
            <div className="column">
                <LocationBadge location={this.props.location} usingDefaultLocation = {this.props.usingDefaultLocation}/>
                <input type="text" placeholder="Hotel name" id="hotelName" onChange={this.props.NameChangeHandler} />
                <input type="text" placeholder="Locality" id="locality"  onChange={this.props.LocalityChangeHandler}/>
            </div>
        )
    }
}

export default TextFilter;
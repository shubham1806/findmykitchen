import React from 'react';

class RatingFilter extends React.Component {
    render() {
        return (
            <div className="column">
                <h3>Rating</h3>
                <div className="FilterContainer">
                    <div className="checkBoxFilter">
                        <input type="checkbox" name="cuisine" id="4Star" value="4" onClick={this.props.ChangeHandler}  />
                        <label htmlFor="4Star">4 & more</label>
                    </div>
                    <div className="checkBoxFilter">
                        <input type="checkbox" name="cuisine" id="3Star" value="3" onClick={this.props.ChangeHandler}  />
                        <label htmlFor="3Star">3 & more</label>
                    </div>
                    <div className="checkBoxFilter">
                        <input type="checkbox" name="cuisine" id="2Star" value="2" onClick={this.props.ChangeHandler}  />
                        <label htmlFor="2Star">2 & more</label>
                    </div>
                    <div className="checkBoxFilter">
                        <input type="checkbox" name="cuisine" id="1Star" value="1" onClick={this.props.ChangeHandler}  />
                        <label htmlFor="1Star">1 & more</label>
                    </div>
                </div>
            </div>
        )
    }
    
}

export default RatingFilter;
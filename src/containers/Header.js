import '../scss/header.scss';
import React from 'react';

class Header extends React.Component {
    render() {
        return (
            <header className="header">
                <a href="/" className="brand">Find My Kitchen</a>
                <ul>
                    <li><a href="https://bitbucket.org/shubham1806/assignment" rel="noreferrer" target="_blank">Go to repo</a></li>
                </ul>
            </header>
        )
    }
}

export default Header;
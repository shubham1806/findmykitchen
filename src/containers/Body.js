import '../scss/body.scss';
import React from 'react';


import TextFilter from '../components/filters/TextFilter';
import RatingFilter from '../components/filters/RatingFilter';
import CuisineFilter from '../components/filters/CuisineFilter';
import RestaurantShowcase from '../components/RestrauntShowcase';

var cuisines = [];

var restaurants = {};

var USER_KEY = "fcf617f76e8b5387bdab21330257a415";

class Body extends React.Component {

    constructor() {
        super();
        this.state = {
            cuisines: [],
            restaurants: [],
            nameFilter: "",
            localityFilter: "",
            cuisinesFilter: [],
            ratingFilter: [],
            loadingResults: false,
            resultsFound: false,
            location: {},
            usingDefaultLocation: false
        }
        this.filterByName = this.filterByName.bind(this);
        this.filterByLocality = this.filterByLocality.bind(this);
        this.filterByCuisine = this.filterByCuisine.bind(this);
        this.setStateSynchronous = this.setStateSynchronous.bind(this);
        this.filterByRating = this.filterByRating.bind(this);
        
    }

    filterByName(event) {
        if(event.target.value !== "") {
            let filteredResults = restaurants.filter(element => {
                return element.restaurant.name.toLowerCase().startsWith(event.target.value.toLowerCase());
            });
            if(filteredResults.length === 0) {
                this.setState({
                    restaurants: filteredResults,
                    nameFilter: event.target.value,
                    resultsFound: false
                });
            } else {
                this.setState({
                    restaurants: filteredResults,
                    nameFilter: event.target.value,
                    resultsFound: true
                });
            }
            
        } else {
            this.setState({
                restaurants: restaurants,
                nameFilter: event.target.value
            })
        }
    }

    filterByLocality(event) {
        if(event.target.value !== "") {
            let filteredResults = restaurants.filter(element => {
                return element.restaurant.location.locality.toLowerCase().startsWith(event.target.value.toLowerCase());
            });
            if(filteredResults.length === 0) {
                this.setState({
                    restaurants: filteredResults,
                    LocalityFilter: event.target.value,
                    resultsFound: false
                });
            } else {
                this.setState({
                    restaurants: filteredResults,
                    localityFilter: event.target.value,
                    resultsFound: true
                });
            }
            
        } else {
            this.setState({
                restaurants: restaurants,
                localityFilter: event.target.value
            })
        }
    }

    setStateSynchronous(stateUpdate) {
        return new Promise(resolve => {
            this.setState(stateUpdate, () => resolve());
        });
    }

    async filterByCuisine(event) {
        
        
        if(event.target.checked) {
            await this.setStateSynchronous({
                cuisinesFilter: [...new Set([...this.state.cuisinesFilter,event.target.value])]
            }).then(() => {
                
                let filteredResults = restaurants.filter(element => {
                    let cuisineCheck = false;
                    element.restaurant.cuisines.split(", ").forEach(cuisineElement => {
                        if(this.state.cuisinesFilter.includes(cuisineElement)) {
                            cuisineCheck = true;
                        }
                        
                    });
                    
                    return cuisineCheck;
                });
                
                if(filteredResults.length === 0) {
                    if(this.state.restaurants.length < restaurants.length) {
                        this.setState({
                            restaurants: [...new Set([...this.state.restaurants,filteredResults])],
                            cuisinesFilter: [...new Set([...this.state.cuisinesFilter,event.target.value])],
                            resultsFound: false
                        });
                    } else {
                        this.setState({
                            cuisinesFilter: [...new Set([...this.state.cuisinesFilter,event.target.value])],
                            resultsFound: false
                        });
                    }
                } else {
                    this.setState({
                        restaurants: filteredResults,
                        cuisinesFilter: [...new Set([...this.state.cuisinesFilter,event.target.value])],
                        resultsFound: true
                    });
                }
            });
        } else {
            
            await this.setStateSynchronous({
                cuisinesFilter: this.state.cuisinesFilter.filter(element => {
                    return element !== event.target.value 
                })
            }).then(() => {
                
                if(this.state.cuisinesFilter.length > 0) {
                    let filteredResults = restaurants.filter(element => {
                        let cuisineCheck = false;
                        
                        element.restaurant.cuisines.split(", ").forEach(cuisineElement => {
                            
                            if(this.state.cuisinesFilter.indexOf(cuisineElement) >= 0) {
                                cuisineCheck = true;
                            }
                            
                        });
                        
                        return cuisineCheck;
                    });
                    if(filteredResults.length === 0) {
                        if(this.state.restaurants.length < restaurants.length) {
                            this.setState({
                                restaurants: [...new Set([...this.state.restaurants,filteredResults])],
                                resultsFound: false
                            });
                        } else {
                            this.setState({
                                
                                resultsFound: false
                            });
                        }
                    } else {
                        this.setState({
                            restaurants: filteredResults,
                            resultsFound: true
                        });
                    }
                } else {
                    this.setState({
                        restaurants: restaurants,
                        resultsFound: true
                    });
                }
                
            })
        }
        
    }

    async filterByRating(event) {
        
        
        if(event.target.checked) {
            await this.setStateSynchronous({
                ratingFilter: [...new Set([...this.state.ratingFilter,event.target.value])]
            }).then(() => {
                
                let filteredResults = restaurants.filter(element => parseFloat(element.restaurant.user_rating.aggregate_rating) >= parseFloat(event.target.value));
                
                if(filteredResults.length === 0) {
                    if(this.state.restaurants.length < restaurants.length) {
                        this.setState({
                            restaurants: [...new Set([...this.state.restaurants,filteredResults])],
                            ratingFilter: [...new Set([...this.state.ratingFilter,event.target.value])],
                            resultsFound: false
                        });
                    } else {
                        this.setState({
                            ratingFilter: [...new Set([...this.state.ratingFilter,event.target.value])],
                            resultsFound: false
                        });
                    }
                } else {
                    this.setState({
                        restaurants: filteredResults,
                        ratingFilter: [...new Set([...this.state.ratingFilter,event.target.value])],
                        resultsFound: true
                    });
                }
            });
        } else {
            
            await this.setStateSynchronous({
                ratingFilter: this.state.ratingFilter.filter(element => {
                    return element !== event.target.value 
                })
            }).then(() => {
                
                if(this.state.ratingFilter.length > 0) {
                    let filteredResults = restaurants.filter(element => parseFloat(element.restaurant.user_rating.aggregate_rating) >= parseFloat(event.target.value));
                    if(filteredResults.length === 0) {
                        if(this.state.restaurants.length < restaurants.length) {
                            this.setState({
                                restaurants: [...new Set([...this.state.restaurants,filteredResults])],
                                resultsFound: false
                            });
                        } else {
                            this.setState({
                                resultsFound: false
                            });
                        }
                    } else {
                        this.setState({
                            restaurants: filteredResults,
                            resultsFound: true
                        });
                    }
                } else {
                    this.setState({
                        restaurants: restaurants,
                        resultsFound: true
                    });
                }
                
            })
        }
        
    }
        

    componentDidMount() {
        if("geolocation" in navigator) {
            console.log("Location available");
        } else {
            console.log("Location not available")
        }
        this.setState({
            loadingResults: true
        });
        navigator.geolocation.getCurrentPosition(position => {
            // console.log("latitude: ", position.coords.latitude);
            // console.log("longitude: ", position.coords.longitude);
            this.setState({
                usingDefaultLocation: false
            });
            fetch("https://developers.zomato.com/api/v2.1/search?lat="+position.coords.latitude+"&lon="+position.coords.longitude+"&radius=100&sort=real_distance&order=asc", {
                headers: {
                    "user-key": USER_KEY
                }
            })
                .then((res) => res.json())
                .then(data => {
                    
                    
                    let allCuisines = [];
                    data.restaurants.forEach(element => {
                        allCuisines.push(...element.restaurant.cuisines.split(", "));
                    });
                    cuisines = [...new Set(allCuisines)];
                    this.setState({
                        cuisines: [...new Set(allCuisines)],
                        restaurants: data.restaurants,
                        loadingResults: false,
                        resultsFound: true
                    });
                    restaurants = data.restaurants;

                });
            },
            error => {
                this.setState({
                    usingDefaultLocation: true
                });
                console.log("Error retrieveing Location: ", error.message);

                //Default coordingates: 28.4953091817,77.0886565000

                fetch("https://developers.zomato.com/api/v2.1/search?lat=28.4953091817&lon=77.0886565000&radius=100&sort=real_distance&order=asc", {
                headers: {
                    "user-key": USER_KEY
                }
            })
                .then((res) => res.json())
                .then(data => {
                    
                    
                    let allCuisines = [];
                    data.restaurants.forEach(element => {
                        allCuisines.push(...element.restaurant.cuisines.split(", "));
                    });
                    cuisines = [...new Set(allCuisines)];
                    this.setState({
                        cuisines: [...new Set(allCuisines)],
                        restaurants: data.restaurants,
                        loadingResults: false,
                        resultsFound: true
                    });
                    restaurants = data.restaurants;

                });
            });
    }

    render() {
        
        if (! this.state.loadingResults) {
            console.log(this.state.restaurants);
            return (
                <div className="content_wrapper">
                    <div className="filter_panel">
                        <TextFilter 
                        location={this.state.restaurants.length > 0 && this.state.resultsFound ? this.state.restaurants[0].restaurant.location.city : ""}
                        usingDefaultLocation = {this.state.usingDefaultLocation}
                        NameChangeHandler={this.filterByName} 
                        LocalityChangeHandler={this.filterByLocality}/>
                        <CuisineFilter 
                        ChangeHandler = {this.filterByCuisine}
                        cuisines = {cuisines}/>
                        <RatingFilter 
                        ChangeHandler = {this.filterByRating}/>
                    </div>
                    <RestaurantShowcase 
                    restaurants={this.state.restaurants}
                    loadingResults={this.state.loadingResults}
                    resultsFound={this.state.resultsFound}/>
                </div>
            )
        } else {
            return (
                <div className="content_wrapper">
                    <div className="LoadingWrapper">
                        <p>Loading ...</p>
                    </div>
                </div>
            )
        }

    }
}

export default Body;
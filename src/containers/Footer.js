import '../scss/footer.scss';
import React from 'react';

class Footer extends React.Component {
    render() {
        return (
            <footer>
                <p>Created by <a href="https://www.linkedin.com/in/shubham-sharma-2491b811a/" rel="noreferrer" target="_blank">Shubham Sharma</a></p>
            </footer>
        )
    }
}

export default Footer;